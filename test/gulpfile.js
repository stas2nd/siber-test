var  gulp = require('gulp'),
less = require('gulp-less'),
browserSync = require('browser-sync'),
concat = require('gulp-concat'),
uglifyJs = require('gulp-uglifyjs'),
cssNano = require('gulp-cssnano'),
rename =  require('gulp-rename'),
autoprefixer = require('gulp-autoprefixer'),
del = require('del'),
imagemin = require('gulp-imagemin'),
pug = require('gulp-pug');

function wrapPipe(taskFn) {
    return function(done) {
      var onSuccess = function() {
        done();
      };
      var onError = function(err) {
        done(err);
      }
      var outStream = taskFn(onSuccess, onError);
      if(outStream && typeof outStream.on === 'function') {
        outStream.on('end', onSuccess);
      }
    }
}

gulp.task('less', wrapPipe(function(success, error) {
    return gulp.src('src/less/**/*.less')
    .pipe(less().on('error', error))
    .pipe(autoprefixer([
        'last 10 versions'
        ], {
          cascade: true
        })
    )
    .pipe(gulp.dest('src/css'));
}));

gulp.task('browser-sync', function() {
browserSync({
    server: {
        baseDir: 'dist'
    },
    tunnel: true,
    host: 'localhost',
    port: 9000,
    logPrefix: 'test'
    });
});

gulp.task('watch', ['browser-sync'], function() {
    gulp.watch('src/less/**/*.less', ['min-css', browserSync.reload]);
    gulp.watch('src/img/**/*.*', ['compress', browserSync.reload]);
    gulp.watch('src/js/**/*.js', ['min-js', browserSync.reload]);
    gulp.watch('src/pug/*.pug', ['html', browserSync.reload]);
});

gulp.task('min-js', function() {
    return gulp.src('src/js/**/*.js')
    .pipe(uglifyJs())
    .pipe(rename({
        suffix: '.min'
      }))
    .pipe(gulp.dest('dist/js'));
});

gulp.task('min-css', ['less'] , function() {
    return gulp.src('src/css/*.css')
    .pipe(cssNano())
    .pipe(rename({
      suffix: '.min'
    }))
    .pipe(gulp.dest('dist/css'));
});

gulp.task('clean', function() {
    return del.sync('dist');
});

gulp.task('compress', function() {
    gulp.src('src/img/**')
    .pipe(imagemin())
    .pipe(gulp.dest('dist/img'));
});

gulp.task('html', function() {
    return gulp.src('src/pug/*.pug')
    .pipe(pug())
    .pipe(gulp.dest('dist'))
});

gulp.task('build', ['clean', 'html', 'compress', 'min-css', 'min-js'], function() {
  
    var buildFonts = gulp.src('src/fonts/**/*')
    .pipe(gulp.dest('dist/fonts'));

});

gulp.task('default', ['build', 'watch']);
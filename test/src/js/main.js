'use strict';
var tool, frame;


function closeHover(el) {
    tool = el.closest('.tooltip');
    frame = tool.closest('.features-block__item');
    tool.style.display = "none";
    frame.style.zIndex = "1";
}

function openHover(el) {
    var flagMob = false;
    var windowW = screen.width;
    if (windowW < 480) {
        flagMob = true;
    }
    if (!el.classList.contains('tooltip__close') && flagMob) {
        tool = el.querySelector('.tooltip');
        if (!tool) {
            frame = el.closest('.features-block__item');
            tool = frame.querySelector('.tooltip');
        } else {
            frame = tool.closest('.features-block__item');
        }
        tool.style.display = "block";
        frame.style.zIndex = "4";
    }
}

var buttonScroll = document.querySelector('.js-scroll-video');

if (buttonScroll) {
    var sect = buttonScroll.closest('section');
    var video = document.querySelector('#video');
    
    buttonScroll.addEventListener('click', function(e) {
        e.preventDefault();
        animateScroll(video, 1000, 'linear', 10, top);
    });
}